== Marcelo Arenas (scribe Mikaël Monet)

f: \Sigma^* \to \NN
FP, #P, 
L \in BPP : algo ptime s.t x\in L implies Pr(A accepts x) \geq 3/4
                           x \notin L implies Pr(A rejects x) \geq 3/4
RP: same thing but if x \notin L then always reject
RP \subseteq NP
not known whether BPP \subseteq NP
If NP \subseteq BPP then NP = RP (classic result)
ZPP = RP \cap coRP

Approximation for f, use a FPRAS. 
A: \sigma^* \times (0,1) \to \NN runs in ptime(|x|,1/epsilon)
for all x in sigma^* and epsilon in (0,1), Pr[|A(x,\epsilon) - f(x)| \leq epsilon f(x)] \geq 3/4

Goal of the talk: give techniques to show that some things do not have fpras
3 approaches.

=== When the associated decision problem is NP-hard 

How to show that #SAT has no fpras unless NP=RP:

L_f = {x \in sigma^* \mid f(x) > 0}

L_#SAT = SAT

What is interesting about #SAT is that some pbs f are #P-complete but yet L_f
is in ptime

Thm: if f admits an FPRAS then L_f is in BPP.
Proof: assume we have such an fpras A for f. Define then Algo B:    (this proof assumes f(x) is positive, the same idea works if it is negative)
run A(x,1/2), if > 0 return YES, if = 0 return NO.
We show that B is a BPP algo:
We have Pr(|A(x,1/2) - f(x)| \leq 1/2 f(x)) \geq 3/4 (*)
 => Pr(1/2f(x) \leq A(x,1/2) \leq 3/2 f(x)) \geq 3/4 (**)
If x \in L_f then by (**) we have Pr(A(x,1/2) > 0) \geq 3/4 so OK
If x \notin L_F by (*) we have Pr(A(x,1/2) = 0) \geq 3/4 so OK
qed

=== #2-CNF. Here the decision problem is ptime

f: \sigma^* \to \NN
g: \sigma^* \to \NN

h is a ptime parsimonious reduction from f to g if:
 - for all x in sigma^*, f(x) = g(h(x))
we write f \leq_pars g

This obviously conserves having an fpras: if we have an fpras for g and a pars
redu from f to g then we have a fpras for f.

We'll use the above in the contrapositive to show that some pbs do not have fpras.

Let's assume for a moment that #IS has no fpras (will show it later), let's
show that #2-CNF has no fpras. We show #IS \leq_pars #2-CNF (obvious reduction)
Also works for #2-monotoneCNFs


Let's show #IS \leq_pars #MIS (number of maximal (under inclusion) independent sets)
(exercise) (easy solution: given the graph, add a dangling node to every node)

Let's now show that #IS does not have a fpras.
Thm: if #IS admits an FPRAS then NP=RP.

define problem GIS = {G,k s.t. G has an IS with k nodes}.

We want to construct a graph G' s.t. if G has an indep set with k nodes then
the #IS is very large and otherwise "small"

Given G and r, construct G^r = (V^r,E^r): replace each node v by r nodes
C_v = {v_1,\ldots,v_r} , connect two nodes u_i and v_j if the original nodes u and v
were connected in G.

Given an IS S of G and T of G^r, we say that T is a witness for S if S = {v \in
V s.t. C_v \cap T \neq \emtpyset}. For such a T define S_T the set that it
witnesses.


L^r(G,k) = {T s.t. T is an IS of G^r s.t. |S_T| = k} (L as in large)
S^r(G,k) = {T s.t. T is an IS of G^r s.t. |S_T| < k} (S as in small)

Let S an IS of G with |S|=l. Call w_l the number of witnesses of S in G^r. Then w_l = (2^r-1)^l
|S^r(G,k)| \leq \sum_{l=0}^{k-1} (l chose n) w_l \leq w_{k-1} \sum_{l=0}^{k-1} (l chose n) = 2^n (2^r-1)^{k-1}

If (G,k) \in GIS |L^r(G,k)| \geq w_k = (2^r-1)^k

Analysis:
- if (G,k) \noting GIS then #IS(G^r) \leq 2^n (2^r-1)^{k-1}
- if (G,k) \in GIS then #IS(G^r) \geq (2^r-1)^k

pick r = n+3.

We have an frpas Pr[|A(G,eps) - #IS(G)| \leq eps #IS(G)] \geq 3/4

We build algo B(G,k):
 - compute G^{n+3}
 - run A(G^{n+3}, 1/2) = s
 - if s \geq 3/2 \times  (2^{n+3}-1)^k then return YES, otherwise return NO.

Do the calculation and check that it works.

Exo: 
#SimpleCycle (count nb of simple cycles (only once)) also has no fpras. (here
the associated hard decision pb we should target is hamiltonian cycle). The
same construction as above could work (Marcelo not sure). We can also replace
every edge by a saussage graph of length r and then chose the correct value of r. 
Same for #SimpleCycle (counting the cycles only once)
